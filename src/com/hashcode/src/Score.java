package com.hashcode.src;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.hashcode.ressource.File;

public class Score {

	DataCenter factory;
	ArrayList<Pool> pool;
	ArrayList<Server> server;
	ArrayList<Row> row;
	Score score ;
	ArrayList<Integer> zwischenresults = new ArrayList<Integer>() ;
	//ArrayList<Server> serveur ;
	public Score(DataCenter factory){
		// factory = new DataCenter(file)
		pool= factory.getPools();
		server= factory.getServers();
		row=factory.getRows();

	}



	public ArrayList<Server> getServers(Pool pool,Row  row){
		int indexPool=pool.getId();
		int indexRow= row.getId();
		ArrayList<Server> serveur = new ArrayList<Server>() ;
		for(int i=0;i<server.size();i++){
			if((server.get(i).getPool()==indexPool)||(server.get(i).getRow()==indexRow)){
				serveur.add(server.get(i));
			}

		}
		return serveur;

	}
	public ArrayList<Integer> getScore(){
		ArrayList<Server> serv;
		int result=0;
		Random rand = new Random(new Date().getTime());
		int value = rand.nextInt(row.size());
		for(int i=0;i<pool.size();i++){

			for(int j=row.size()-1;j>-1;j--){
				if(value!=j){
					//DEBUG System.out.println(">>" + score) ;
					serv=this.getServers(pool.get(i), row.get(j));
					for(int k=0;k<serv.size();k++){
						result+=serv.get(k).getCapacity();
					}

					zwischenresults.add(result);
					result=0;
				}
			}
		}
		return zwischenresults;
	}
	public int getGuaranteedcap(){
		//System.out.println("HHH") ;
		int gtc= 312+getScore().get(0) - 312;
		for(int i=1;i<row.size();i++){
			if(getScore().get(i-1)>getScore().get(i)){
				gtc=getScore().get(i);
				//System.out.println("HHH") ;
			}
		}
		//System.out.println("HHH") ;
		return gtc;
	}

}



