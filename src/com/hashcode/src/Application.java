package com.hashcode.src;

import java.util.ArrayList;
import java.util.Iterator;

import com.hashcode.ressource.File;
import com.hashcode.ressource.FileManager;

public class Application {

	public static void main(String[] args) {

		FileManager fm = new FileManager();

		DataCenter datacenter = new DataCenter(fm.getFile("input.txt"));

		datacenter.run();

		// Score
		Score sc = new Score(datacenter) ;
		System.out.println("Score : " + sc.getGuaranteedcap());
		ArrayList<Server> serveurs = datacenter.getServers();

		File output = new File();

		Iterator<Server> itServer = serveurs.iterator();
		int t = 0 ;
		while (itServer.hasNext()) {
			Server temp = itServer.next();
			if(temp.getRow() >= 0 && temp.getSlot() >= 0){
				t += temp.getCapacity() ;
				output.addLine(temp.getRow() + " " + temp.getSlot() + " " + temp.getPool());
			}else
				output.addLine("x");
		}
		System.out.println("Total capacity : " + t) ;

		fm.printFile(output, "output.txt");

	}
}
