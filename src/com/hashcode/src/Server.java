package com.hashcode.src;


public class Server {

	int ratio=0;
	int id=0;
	int size=0;
	int capacity=0;
	int pool=-1;
	int row=-1;
	int slot=-1;
	public Server(int capacity, int size,int id){
		// DEBUG System.out.println("INIT SERV : " + capacity + " / " + size + " / " + id);
		this.ratio= size/capacity;
		this.capacity=capacity;
		this.size=size;
		this.id = id ;

	}
	public int getRatio() {
		return ratio;
	}
	public void setRatio(int ratio) {
		this.ratio = ratio;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getPool() {
		return pool;
	}

	public void setPool(int pool) {
		this.pool = pool;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row ;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

}
