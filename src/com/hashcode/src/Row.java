package com.hashcode.src;

import java.util.ArrayList;
import java.util.Iterator;

public class Row {

	ArrayList<Slot> listOfSlots;
	int id ;
	public Row(int id){
		this.id=id ;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Row() {
		listOfSlots = new ArrayList<Slot>();
	}

	public ArrayList<Slot> getSlots() {
		return listOfSlots;
	}

	public void setSlots(ArrayList<Slot> listOfSlots) {
		this.listOfSlots = listOfSlots;
	}

	public int getCasesConsecutives() {
		// DEBUG System.out.println("HIER = " + listOfSlots.size()) ;
		int i = 0;
		int m = 0;
		for (Slot s : listOfSlots) {
			// DEBUG System.out.println("Avail ? " + s.available);
			if (s.available) {

				i++;
			} else {
				if (m < i) {
					m = i;
				}
				i = 0;
			}
		}
		if (i > m)
			return i;
		return m;
	}

	public int getIndexOfNCases(int n) {
		int i = 0;
		int p = 0;
		boolean found = false;
		Iterator<Slot> itSlot = listOfSlots.iterator();
		while (!found && itSlot.hasNext()) {
			i++;
			Slot temp = itSlot.next();

			if (i == n) {
				found = true;
				p = temp.getId() - n + 1;
			}
			if (!temp.isAvailable())
				i = 0;

		}
		if (found)
			return p;
		return -1;

	}
}
