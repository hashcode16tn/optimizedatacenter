package com.hashcode.src;

import java.util.ArrayList;
import java.util.Comparator;

import com.hashcode.ressource.File;
import com.hashcode.src.Pool;
import com.hashcode.src.Row;
import com.hashcode.src.Server;

public class DataCenter {
	ArrayList<Server> servers = new ArrayList<Server>();
	ArrayList<Pool> pools = new ArrayList<Pool>();
	ArrayList<Row> rows = new ArrayList<Row>();

	ArrayList<Server> _sortedServers = new ArrayList<Server>();

	public DataCenter(File f) {
		String[] params = f.getLine(0).split(" ");
		Integer nbOfRows = new Integer(params[0]);
		Integer nbOfSlots = new Integer(params[1]);
		Integer nbOfInvalids = new Integer(params[2]);
		Integer nbOfPools = new Integer(params[3]);
		Integer nbOfServers = new Integer(params[4]);
		Integer lineCursor = 1;
		ArrayList<Row> _rows = new ArrayList<Row>();
		for (int i = 0; i < nbOfRows; i++) {
			Row r = new Row(i);
			ArrayList<Slot> slots = new ArrayList<Slot>();
			for (int j = 0; j < nbOfSlots; j++) {
				slots.add(new Slot(j));

			}
			r.setSlots(slots);
			_rows.add(r);
		}

		for (int i = 0; i < nbOfInvalids; i++) {
			String line = f.getLine(i + lineCursor);
			params = line.split(" ");
			Integer row = new Integer(params[0]);
			Integer slot = new Integer(params[1]);
			_rows.get(row).getSlots().get(slot).setAvailability(false);
		}

		lineCursor = nbOfInvalids + 1;
		ArrayList<Server> _servers = new ArrayList<Server>();
		for (int i = 0; i < nbOfServers; i++) {
			String line = f.getLine(i + lineCursor);
			params = line.split(" ");
			// DEBUG System.out.println(params[0]) ;
			Server s = new Server(new Integer(params[0]),new Integer(params[1]), i);
			_servers.add(s);
		}

		for (int i = 0; i < nbOfPools; i++) {
			pools.add(new Pool(i));
		}
		servers = _servers;
		rows = _rows;

	}

	public void run() {
		Comparator<Server> comp = new ServerComparator();
		_sortedServers = new ArrayList<Server>(servers);
		_sortedServers.sort(comp);

		/*for(Server s : _sortedServers){
			System.out.println(" >> " + s.getId() + " => " + s.getRatio() + " // " + s.getSize() + " // " + s.getCapacity()) ;
		}*/
		// DEBUG System.out.println("B" + servers.get(0).getId());
		// DEBUG System.out.println("A" + _sortedServers.get(0).getId());

		placeServers() ;

		// assign pools
		assignPools();


		// Score
		 //get_score() ;
	}

	private Server getBestServerInRow(Row row) {
		// DEBUG System.out.println("Entering best server in row : " + row.getId());
		int c = row.getCasesConsecutives();
		// DEBUG System.out.println("C = " + c) ;
		Server bestServ = null;
		for (Server s : _sortedServers) {
			if (s.getSize() <= c) {
				// DEBUG System.out.println("Best found") ;
				bestServ = s;
				_sortedServers.remove(s) ;
				break;
			}
		}

		return bestServ;
	}

	private void assignPools() {
		Integer nbOfPools = pools.size();
		Integer p = 0;
		for (Row r : rows) {
			ArrayList<Server> serversInRow = getServersInRow(r);
			for (Server s : serversInRow) {
				s.setPool(p);
				p = ((p + 1) % nbOfPools);
			}
		}
	}

	private ArrayList<Server> getServersInRow(Row r) {
		ArrayList<Server> listServers = new ArrayList<Server>();
		for (Server s : servers) {
			if (s.getRow() == r.getId()) {
				listServers.add(s);
			}
		}
		return listServers;
	}

	private void placeServer(Server s, Row r, Integer i) {
		s.setRow(r.getId());
		s.setSlot(i);
		r.getSlots().get(i).setAvailability(false);
	}

	/**
	 * @return the servers
	 */
	public ArrayList<Server> getServers() {
		return servers;
	}

	/**
	 * @param servers
	 *            the servers to set
	 */
	public void setServers(ArrayList<Server> servers) {
		this.servers = servers;
	}

	/**
	 * @return the pools
	 */
	public ArrayList<Pool> getPools() {
		return pools;
	}

	/**
	 * @param pools
	 *            the pools to set
	 */
	public void setPools(ArrayList<Pool> pools) {
		this.pools = pools;
	}

	/**
	 * @return the rows
	 */
	public ArrayList<Row> getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(ArrayList<Row> rows) {
		this.rows = rows;
	}

	public void placeServers() {
		// DEBUG System.out.println("Entering place servers");
		// DEBUGSystem.out.println("Full ? " + areFull());
		// DEBUG System.out.println("Empty? " + _sortedServers.isEmpty()) ;

		while (!areFull() && !_sortedServers.isEmpty()) {
			// DEBUG System.out.println("Entring while") ;
			for (Row r : rows) {
				Server s = getBestServerInRow(r);
				// DEBUG System.out.println("Server " + s.getId()) ;
				if(s != null)
					placeServer(s, r, r.getIndexOfNCases(s.getSize()));
			}
		}

	}

	public boolean areFull() {
		for (Row r : rows) {
			if (getBestServerInRow(r) != null)
				return false;
		}
		return true;
	}

	public void get_score(){
 	   ArrayList<Integer> min_range = new ArrayList<Integer>() ;
 		        for(Row row : rows){
 		            ArrayList<Integer> min_pools = new ArrayList<Integer>() ;
 		            for(int i = 0 ; i < pools.size() ; i++){
 		            	min_pools.add(0) ;
 		            }

 		            for( Server srv : servers){
 		                if (srv.getSlot() >= 0 && srv.getRow() != row.getId()){
 		                    min_pools.add(min_pools.get(srv.getPool()) + srv.getCapacity()); // += srv.getCapacity() ;
 		                }
 		            }
 		            min_range.add((min_pools.get(0)));
 		        }
 		        System.out.println(min_range.get(0)) ;
 }
}
