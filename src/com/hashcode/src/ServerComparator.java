package com.hashcode.src;
import java.util.Comparator;



public class ServerComparator implements Comparator<Server> {

	@Override
	public int compare(Server server1, Server server2) {
		int comp=0 ;
		if(server1.getRatio() > server2.getRatio()){
			comp =-1 ;
		}
		if(server1.getRatio()<server2.getRatio()){
			comp = 1 ;
		}
		if(server1.getRatio()==server2.getRatio() ){
			if(server1.getCapacity() > server2.getCapacity())
				comp = -1 ;
			else if(server1.getCapacity() < server2.getCapacity())
				comp = 1 ;
			else
				comp = 0 ;
		}
		// TODO Auto-generated method stub
		return comp;
	}


}
