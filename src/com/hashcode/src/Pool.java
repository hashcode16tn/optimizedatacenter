package com.hashcode.src;

import java.util.ArrayList;

public class Pool {
	ArrayList<Server> pool= null;
	int id ;
	
	
	public Pool(int id){
		this.id=id ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Server> getPool() {
		return pool;
	}

	public void setPool(ArrayList<Server> pool) {
		this.pool = pool;
	}

}
