package com.hashcode.src;

public class Slot {

	boolean available;
	int id ;
	public Slot(int id ){
		this.id=id ;
		this.available = true ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Slot() {
		available = true;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailability(boolean available) {
		this.available = available;
	}
}
